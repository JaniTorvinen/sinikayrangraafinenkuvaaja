const PiirraKuvaaja     = document.getElementById("PiirraKuvaaja");

let Alkuarvo    = document.getElementById("Alkuarvo");
let Loppuarvo   = document.getElementById("Loppuarvo");
let Step        = document.getElementById("Step");

let xValues = [];
let yValues = [];

PiirraKuvaaja.addEventListener("click", MuodostaKaavio);

function MuodostaKaavio()
{
  let value     = "Math.sin(x)";
  let i1        = parseFloat(Alkuarvo.value);
  let i2        = parseFloat(Loppuarvo.value);
  let step      = parseFloat(Step.value);

  xValues = [];
  yValues = [];  

    for (let x = i1; x <= i2; x += step) 
    {
          yValues.push(eval(value));
          xValues.push(x);
    }
   
   new Chart("myChart", {
    type: "line",
    data: {
      labels: xValues,
      datasets: [{
        fill: false,
        pointRadius: 1,
        borderColor: "rgba(255,0,0,0.5)",
        data: yValues
      }]
    },    
    options: {
      legend: {display: false},
      title: {
        display: true,
        text: "Sinikäyrän graafinen kuvaaja",
        fontSize: 16
      }
    }
  });
 
}

